# Using-Color-Drawable-Resources-in-Java-Code
Using Color-Drawable Resources in Java Code
// Get a Drawable
ColorDrawable redDrawable =
(ColorDrawable)
activity.getResources().getDrawable(R.drawable.red_rctangle);

//Set as a background to a text view
textView.setBackground(redDrawable);
